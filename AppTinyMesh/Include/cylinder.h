#pragma once

#include <iostream>
#include <vector>

#include "mathematics.h"
// Cylinder volumic representation
class Cylinder{
protected:
    double radius;
    double height;
    Vector center;
public:
    Cylinder();
    Cylinder(double radius, double height);
    Cylinder(const Vector& center, double radius, double height);
    Cylinder(const std::vector<double>& center, double radius, double height);

    double getHeight() const;
    double getRadius() const;
    Vector getCenter() const;

    Vector parametrise(double alpha, double y) const;
};