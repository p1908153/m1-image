#pragma once
#include <vector>
#include <iostream>

#include "mathematics.h"
// Sphere volumic representation
class Sphere{
protected:
    Vector center;
    double radius;
public:
    Sphere(double = 1.0);
    Sphere(const Vector&, double = 1.0);
    Sphere(const std::vector<int>&, double);
    Vector parametrise(double u, double v) const;
    double getRadius() const;
    Vector getCenter() const;
};