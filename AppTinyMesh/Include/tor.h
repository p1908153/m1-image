#pragma once

#include <vector>
#include <iostream>

#include "mathematics.h"
// Tor volumic representation
class Tor{
protected:
    Vector center;
    double radius;
    double thickness;

public:
    Tor(double, double);
    Tor(const Vector&, double, double);
    Tor(const std::vector<int>&, double, double);
    Vector parametrise(double u, double v) const;
    double getRadius() const;
    Vector getCenter() const;
};
