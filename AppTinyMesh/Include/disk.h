
#pragma once

#include <iostream>
#include <vector>

#include "mathematics.h"

// Disk volumic representation
class Disk{
protected:
    Vector center;
    double radius;
public:
    Disk(const Vector& center = Vector(0.0,0.0,0.0), double radius = 1.0);
    Disk(const std::vector<double>&, double radius = 1.0);

    double getRadius() const;
    Vector getCenter() const;

    Vector parametrise(double alpha) const;
};