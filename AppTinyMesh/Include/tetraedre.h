
#pragma once

#include <vector>
#include <iostream>

#include "mathematics.h"
// Tetraedre volumic representation
class Tetraedre {
protected:
Vector a,b,c,d;
  
public:
  Tetraedre(Vector, Vector, Vector, Vector);
  Tetraedre();
  Tetraedre(Vector center, double size = 1.0);

  Vector getA() const;
  Vector getB() const;
  Vector getC() const;
  Vector getD() const;
};