#ifndef __Qte__
#define __Qte__

#include <QtWidgets/qmainwindow.h>
#include "realtime.h"
#include "meshcolor.h"
#include "chrono"


QT_BEGIN_NAMESPACE
	namespace Ui { class Assets; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
  Q_OBJECT
private:
  Ui::Assets* uiw;           //!< Interface

  MeshWidget* meshWidget;   //!< Viewer
  MeshColor meshColor;		//!< Mesh.
  

public:
  MainWindow();
  ~MainWindow();
  void CreateActions();
  void UpdateGeometry();

public slots:
  void editingSceneLeft(const Ray&);
  void editingSceneRight(const Ray&);
  void BoxMeshExample();
  void TetraedreMeshExample();
  void SphereImplicitExample();
  void SphereMeshExample();
  void WarpSiffletMeshExample();
  void WarpSiffletDebugMeshExample();
  void SnowManMeshExample();
  void BoxFractalMeshExample();
  void TorMeshExample();
  void CylinderMeshExample();
  void CapsuleMeshExample();
  void DiskMeshExample();
  void TerrainMeshExample();
  void ResetCamera();
  void UpdateMaterial();
  void setGain(int);
  void setFrequency(int);
  void setOctave(int);
  void setSeed(QString);
  void setLacunarity(int);
  void setAltitude(int);
  void setPrecision(int);
  void BoxVerticesDebug();
  void TetraNormalsDebug();
  void setPx(double);
  void setPy(double);
  void setPz(double);
  void setCx(double);
  void setCy(double);
  void setCz(double);
  void setRayon(double);
  void setFacteurCouleur(QString value);
  void setCouleur1(QString value);
  void setCouleur2(QString value);

  void applySphereWarp();
  void applyFlattening();
  void addSphere();
  void Translate();
  void Scale();
};

#endif
