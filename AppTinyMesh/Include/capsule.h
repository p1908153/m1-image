#pragma once
#include "mathematics.h"
#include <vector>

// Capsule volumic representation
class Capsule
{
protected:
    Vector center;
    double radius;
    double height;

public:
    Capsule(double, double);
    Capsule(const Vector&, double, double);
    Capsule(const std::vector<int>&, double, double);
    double getRadius() const;
    double getHeight() const;
    Vector getCenter() const;
};