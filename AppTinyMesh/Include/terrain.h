#pragma once
#include "mathematics.h"

// Terrain volumic representation
class Terrain
{
private:
    unsigned int w, h, a;
    Vector center;
public:
    Terrain(unsigned int w, unsigned int h, double altitude, const Vector & center);
    ~Terrain();

    unsigned int getWidth() const;
    unsigned int getHeight() const;
    double getAltitude() const;
    Vector getCenter() const;

    void setWidth(unsigned int width);
    void setHeight(unsigned int height);
    void setCenter(const Vector & center);
    void setAltitude(double altitude);
};
