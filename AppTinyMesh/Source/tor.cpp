#include "tor.h"
/**
 * @class Tor tor.h
 * @brief Tor volumic representation.
 */

/// @brief Construct a new Tor object.
/// @param c Center of the torus.
/// @param r Radius of the torus.
/// @param t Thickness of the torus. 
Tor::Tor(const Vector & c, double r, double t) : center(c), radius(r), thickness(t){}


/// @brief Construct a new Tor object with a center of (0,0,0).
/// @param r Radius of the torus.
/// @param t Thickness of the torus. 
Tor::Tor(double r, double t) : center(Vector(0.0,0.0,0.0)), radius(r), thickness(t){}

/// @brief Construct a new Tor object.
/// @param c Center of the torus.
/// @param r Radius of the torus.
/// @param t Thickness of the torus. 
Tor::Tor(const std::vector<int> & c, double r, double t) : radius(r), thickness(t){
    for (int i=0; i<3; i++){
        center[i] = c[i];
    }
}

/// @brief Get the radium of the torus.
/// @return double radius.
double Tor::getRadius() const{
    return radius;
}
/**
 * @brief Get a point at a X Y Z coordinate with an angle and a height 
 * @param u angle in degree
 * @param v angle in degree
 * @return Vector that belongs to the sphere
*/
/// @brief Get a point at a X Y Z coordinate with two angles
/// @param u angle in degree
/// @param v angle in degree
/// @return Vector that belongs to the torus
Vector Tor::parametrise(double u, double v) const
{
    return Vector((radius + thickness * cos(v)) * cos(u), thickness * sin(v), (radius + thickness * cos(v)) * sin(u));
}

/// @brief Get the center of the torus.
/// @return Vector center.
Vector Tor::getCenter() const
{
    return center;
}