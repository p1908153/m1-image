#include "sphere.h"

/**
 * @class Sphere sphere.h
 * @brief Sphere volumic representation.
 */

/**
 * @brief Construct a new Sphere object
 * 
 * @param c Center of the sphere
 * @param r Radius of the sphere
 */
Sphere::Sphere(const Vector & c, double r) : center(c), radius(r){}


/**
 * @brief Construct a new Sphere object with a center of (0,0,0)
 * 
 * @param r Radius of the sphere
 */
Sphere::Sphere(double r) : center(Vector(0.0,0.0,0.0)), radius(r){}

/**
 * @brief Construct a new Sphere object
 * 
 * @param c Center of the sphere
 * @param r Radius of the sphere
 */
Sphere::Sphere(const std::vector<int> & c, double r) : radius(r){
    for (int i=0; i<3; i++){
        center[i] = c[i];
    }
}

/**
 * @brief Get the height of the sphere
 * 
 * @return double Height
 */
double Sphere::getRadius() const{
    return radius;
}

/**
 * @brief Get a point at a X Y Z coordinate with two angles.
 * @param u angle in degree
 * @param v angle in degree
 * @return Vector that belongs to the sphere
*/
Vector Sphere::parametrise(double u, double v) const
{
    return Vector(radius * cos(u) * cos(v), radius * sin(u), radius * cos(u) * sin(v));
}

/**
 * @brief Get the center of the sphere
 * 
 * @return Vector center
 */
Vector Sphere::getCenter() const
{
    return center;
}
