#include "mesh.h"
/*!
\class Mesh mesh.h

\brief Core triangle mesh class.
*/



/*!
\brief Initialize the mesh to empty.
*/
Mesh::Mesh()
{
}

/*!
\brief Initialize the mesh from a list of vertices and a list of triangles.

Indices must have a size multiple of three (three for triangle vertices and three for triangle normals).

\param vertices List of geometry vertices.
\param indices List of indices wich represent the geometry triangles.
*/
Mesh::Mesh(const std::vector<Vector>& vertices, const std::vector<int>& indices) :vertices(vertices), varray(indices)
{
  normals.resize(vertices.size(), Vector::Z);
}

/*!
\brief Create the mesh.

\param vertices Array of vertices.
\param normals Array of normals.
\param va, na Array of vertex and normal indexes.
*/
Mesh::Mesh(const std::vector<Vector>& vertices, const std::vector<Vector>& normals, const std::vector<int>& va, const std::vector<int>& na) :vertices(vertices), normals(normals), varray(va), narray(na)
{
}

/*!
\brief Reserve memory for arrays.
\param nv,nn,nvi,nvn Number of vertices, normals, vertex indexes and vertex normals.
*/
void Mesh::Reserve(int nv, int nn, int nvi, int nvn)
{
  vertices.reserve(nv);
  normals.reserve(nn);
  varray.reserve(nvi);
  narray.reserve(nvn);
}

/*!
\brief Empty
*/
Mesh::~Mesh()
{
}

/*!
\brief Smooth the normals of the mesh.

This function weights the normals of the faces by their corresponding area.
\sa Triangle::AreaNormal()
*/
void Mesh::SmoothNormals()
{
  // Initialize 
  normals.resize(vertices.size(), Vector::Null);

  narray = varray;

  // Accumulate normals
  for (int i = 0; i < varray.size(); i += 3)
  {
    Vector tn = Triangle(vertices[varray.at(i)], vertices[varray.at(i + 1)], vertices[varray.at(i + 2)]).AreaNormal();
    normals[narray[i + 0]] += tn;
    normals[narray[i + 1]] += tn;
    normals[narray[i + 2]] += tn;
  }

  // Normalize 
  for (int i = 0; i < normals.size(); i++)
  {
    Normalize(normals[i]);
  }
}

/*!
\brief Add a smooth triangle to the geometry.
\param a, b, c Index of the vertices.
\param na, nb, nc Index of the normals.
*/
void Mesh::AddSmoothTriangle(int a, int na, int b, int nb, int c, int nc)
{
  varray.push_back(a);
  narray.push_back(na);
  varray.push_back(b);
  narray.push_back(nb);
  varray.push_back(c);
  narray.push_back(nc);
}

/*!
\brief Add a triangle to the geometry.
\param a, b, c Index of the vertices.
\param n Index of the normal.
*/
void Mesh::AddTriangle(int a, int b, int c, int n)
{
  varray.push_back(a);
  narray.push_back(n);
  varray.push_back(b);
  narray.push_back(n);
  varray.push_back(c);
  narray.push_back(n);
}

/*!
\brief Add a smmoth quadrangle to the geometry.

Creates two smooth triangles abc and acd.

\param a, b, c, d  Index of the vertices.
\param na, nb, nc, nd Index of the normal for all vertices.
*/
void Mesh::AddSmoothQuadrangle(int a, int na, int b, int nb, int c, int nc, int d, int nd)
{
  // First triangle
  AddSmoothTriangle(a, na, b, nb, c, nc);

  // Second triangle
  AddSmoothTriangle(a, na, c, nc, d, nd);
}

/*!
\brief Add a quadrangle to the geometry.

\param a, b, c, d  Index of the vertices and normals.
*/
void Mesh::AddQuadrangle(int a, int b, int c, int d)
{
  AddSmoothQuadrangle(a, a, b, b, c, c, d, d);
}

/*!
\brief Compute the bounding box of the object.
*/
Box Mesh::GetBox() const
{
  if (vertices.size() == 0)
  {
    return Box::Null;
  }
  return Box(vertices);
}

/*!
\brief Creates an axis aligned box.

The object has 8 vertices, 6 normals and 12 triangles.
\param box The box.
*/
Mesh::Mesh(const Box& box)
{
  // Vertices
  vertices.resize(8);

  for (int i = 0; i < 8; i++)
  {
    vertices[i] = box.Vertex(i);
  }

  // Normals
  normals.push_back(Vector(-1, 0, 0));
  normals.push_back(Vector(1, 0, 0));
  normals.push_back(Vector(0, -1, 0));
  normals.push_back(Vector(0, 1, 0));
  normals.push_back(Vector(0, 0, -1));
  normals.push_back(Vector(0, 0, 1));

  // Reserve space for the triangle array
  varray.reserve(12 * 3);
  narray.reserve(12 * 3);

  AddTriangle(0, 2, 1, 4);
  AddTriangle(1, 2, 3, 4);

  AddTriangle(4, 5, 6, 5);
  AddTriangle(5, 7, 6, 5);

  AddTriangle(0, 4, 2, 0);
  AddTriangle(4, 6, 2, 0);

  AddTriangle(1, 3, 5, 1);
  AddTriangle(3, 7, 5, 1);

  AddTriangle(0, 1, 5, 2);
  AddTriangle(0, 5, 4, 2);

  AddTriangle(3, 2, 7, 3);
  AddTriangle(6, 7, 2, 3);
}

/**
 * @brief Construct a new tetrahedre Mesh
 * 
 * @param tetra Tetrahedre with needed data
 */
Mesh::Mesh(const Tetraedre& tetra){
  vertices.push_back(tetra.getA());
  vertices.push_back(tetra.getB());
  vertices.push_back(tetra.getC());
  vertices.push_back(tetra.getD());

  normals.push_back(Normalized((vertices[1] - vertices[0]) / (vertices[2] - vertices[0])));
  normals.push_back(Normalized((vertices[1] - vertices[3]) / (vertices[0] - vertices[3])));
  normals.push_back(Normalized((vertices[2] - vertices[0]) / (vertices[3] - vertices[0])));
  normals.push_back(Normalized((vertices[2] - vertices[3]) / (vertices[1] - vertices[3])));

  AddTriangle(0, 1, 2, 0);
  AddTriangle(3, 1, 0, 1);
  AddTriangle(0, 2, 3, 2);
  AddTriangle(3, 2, 1, 3);
}
 
 /**
  * @brief Construct a new Sphere Mesh object
  * 
  * @param s Sphere with needed data
  * @param accuracy Accuracy which is how many point will be in the sphere (Accuracy * Accuracy/2)
  * @param isHalf If true, the Sphere is cut in half
  */
Mesh::Mesh(const Sphere& s, unsigned int accuracy, bool isHalf){
    int divA = accuracy / 2;
    double A, B; // On definit les angles
    for (int i = 0; i <= accuracy / 2; i++)
    {
        A = (double)i * M_PI / divA; //Angle actuelen i
        if(!isHalf) {
          A += -0.5 * M_PI;
        }
        for (int j = 0; j < accuracy; j++)
        {
            B = (double)(j) * 2.f * M_PI / accuracy;//Angle en j
            Vector param = s.parametrise(A, B);
            vertices.push_back(param + s.getCenter());
            normals.push_back(param);
            int k = vertices.size();
            if(i < accuracy/2)
            {
              if(j < accuracy - 1)
              {
                AddSmoothTriangle(k - 1, k - 1,
                  k + accuracy - 1, k + accuracy - 1,
                  k + accuracy, k + accuracy);
                AddSmoothTriangle(k - 1, k - 1,
                  k + accuracy, k + accuracy,
                  k, k);
              }
              else if(j == accuracy - 1)
              {
                AddSmoothTriangle(k - 1, k - 1,
                  k + accuracy - 1, k + accuracy - 1,
                  k + accuracy - j - 1, k + accuracy - j - 1);
                AddSmoothTriangle(k - 1, k - 1,
                  k + accuracy - j - 1, k + accuracy - j - 1,
                  k - j - 1, k - j - 1);
              }
            }
        }
    }
}

/**
 * @brief Construct a new Disk Mesh object
 * 
 * @param d Disk with needed data
 * @param accuracy Accuracy which is how many point will be in the Disk
 */
Mesh::Mesh(const Disk & d, unsigned int accuracy){

    double alpha;
    double step = 2 * M_PI / accuracy;
    normals.push_back(Vector(0, 0, -1));

    vertices.push_back(d.getCenter());

    for (int i = 0; i <= accuracy; i++)
    {
        alpha = i * step;

        normals.push_back(d.parametrise(alpha));
        vertices.push_back(d.parametrise(alpha));

        if(vertices.size() >= 3)
        {
          AddSmoothTriangle(0, 0,
            vertices.size() - 2, normals.size() - 2,
            vertices.size() - 1, normals.size() - 1);
        }
    }
}

/**
 * @brief Construct a new Cylinder Mesh object
 * 
 * @param c Cylinder with needed data
 * @param accuracy Accuracy which is how many point will be in the Cylinder
 * @param open If true, the cylinder will have no disk to close it
 */
Mesh::Mesh(const Cylinder& c, unsigned int accuracy, bool open){
    double step = 2 * M_PI / accuracy;

    double alpha;
    vertices.push_back(Vector(c.getCenter().x(), -c.getHeight()/2.0, c.getCenter().z()));
    normals.push_back(Vector(0, 0, 1));

    vertices.push_back(Vector(c.getCenter().x(), c.getHeight()/2.0, c.getCenter().z()));
    normals.push_back(Vector(0, 0, -1));

    for (int i = 0; i <= accuracy; i++)
    {
        alpha = i * step;

        normals.push_back(c.parametrise(alpha,0));
        vertices.push_back(c.parametrise(alpha,-c.getHeight()/2.0) + c.getCenter());

        normals.push_back(c.parametrise(alpha,0));
        vertices.push_back(c.parametrise(alpha,c.getHeight()/2.0) + c.getCenter());

        if(vertices.size() >= 6)
        {
          AddSmoothTriangle(vertices.size() - 1, normals.size() - 1,
            vertices.size() - 2, normals.size() - 2, 
            vertices.size() - 3, normals.size() - 3);

          AddSmoothTriangle(vertices.size() - 4, normals.size() - 4,
            vertices.size() - 3, normals.size() - 3,
            vertices.size() - 2, normals.size() - 2);
          if(!open)
          {
            AddSmoothTriangle(vertices.size() - 1, normals.size() - 1,
            1, 1, 
            vertices.size() - 3, normals.size() - 3);
            AddSmoothTriangle(vertices.size() - 4, normals.size() - 4,
              0, 0,
              vertices.size() - 2, normals.size() - 2);
          }
          
        }
    }
}

/**
 * @brief Construct a Capsule Mesh object
 * 
 * @param c Capsule with needed data
 * @param accuracy Number of point in each part
 */
Mesh::Mesh(const Capsule& c, unsigned int accuracy){
    this->merge(Mesh(Cylinder(c.getCenter(), c.getRadius(), c.getHeight()), accuracy, true));
    this->merge(Mesh(Sphere(Vector(0, c.getHeight()/2, 0) + c.getCenter(), c.getRadius()), accuracy, true));
    this->merge(Mesh(Sphere(Vector(0, 0, 0), c.getRadius()), accuracy, true).RotateX(180).Translate(Vector(0, -c.getHeight()/2.0, 0) + c.getCenter()));
}

/**
 * @brief Construct a new Tor Mesh object
 * 
 * @param t Tor with needed data
 * @param accuracy Accuracy which is how many point will be in the Tor (Accuracy * Accuracy/2)
 */
Mesh::Mesh(const Tor& t, unsigned int accuracy){
    int divA = accuracy / 2;
    double A, B; // On definit les angles
    for (int i = 0; i <= accuracy / 2; i++)
    {
        A = (double)i * M_PI * 2.0 / divA; //Angle actuelen i
        for (int j = 0; j < accuracy; j++)
        {
            B = (double)(j) * 2.f * M_PI / accuracy;//Angle en j

            Vector param = t.parametrise(A, B);
            vertices.push_back(param + t.getCenter());
            normals.push_back(param);
            int k = vertices.size();
            if(i <= accuracy/2 - 1)
            {
              if(j < accuracy - 1)
              {
                AddSmoothTriangle(k - 1, k - 1,
                  (k + accuracy - 1), (k + accuracy - 1),
                  (k + accuracy), (k + accuracy));
                AddSmoothTriangle(k - 1, k - 1,
                  (k + accuracy), (k + accuracy),
                  k, k);
              }
              else if(j == accuracy - 1)
              {
                AddSmoothTriangle(k - 1, k - 1,
                  (k + accuracy - 1), (k + accuracy - 1),
                  (k + accuracy - j - 1), (k + accuracy - j - 1));
                AddSmoothTriangle(k - 1, k - 1,
                  (k + accuracy - j - 1), (k + accuracy - j - 1),
                  (k - j - 1), (k - j - 1));
              }
            }
        }
    }
}

/*!
\brief Scale the mesh.
\param s Scaling factor.
*/
void Mesh::Scale(double s)
{
    // Vertexes
    for (int i = 0; i < vertices.size(); i++)
    {
        vertices[i] *= s;
    }

    if (s < 0.0)
    {
        // Normals
        for (int i = 0; i < normals.size(); i++)
        {
            normals[i] = -normals[i];
        }
    }
}

/**
 * @brief Translate all vertices by a vector
 * 
 * @param vec Vector to translate by
 * @return Mesh& reference to the current mesh
 */
Mesh& Mesh::Translate(const Vector & vec)
{
    // Vertexes
    for (int i = 0; i < vertices.size(); i++)
    {
        vertices[i] = vertices[i] + vec;
    }
    return (*this);
}



#include <QtCore/QFile>
#include <QtCore/QTextStream>
#include <QtCore/QRegularExpression>
#include <QtCore/qstring.h>

/*!
\brief Import a mesh from an .obj file.
\param filename File name.
*/
void Mesh::Load(const QString& filename)
{
  vertices.clear();
  normals.clear();
  varray.clear();
  narray.clear();

  QFile data(filename);

  if (!data.open(QFile::ReadOnly))
    return;
  QTextStream in(&data);

  // Set of regular expressions : Vertex, Normal, Triangle
  QRegularExpression rexv("v\\s*([-|+|\\s]\\d*\\.\\d+)\\s*([-|+|\\s]\\d*\\.\\d+)\\s*([-|+|\\s]\\d*\\.\\d+)");
  QRegularExpression rexn("vn\\s*([-|+|\\s]\\d*\\.\\d+)\\s*([-|+|\\s]\\d*\\.\\d+)\\s*([-|+|\\s]\\d*\\.\\d+)");
  QRegularExpression rext("f\\s*(\\d*)/\\d*/(\\d*)\\s*(\\d*)/\\d*/(\\d*)\\s*(\\d*)/\\d*/(\\d*)");
  while (!in.atEnd())
  {
    QString line = in.readLine();
    QRegularExpressionMatch match = rexv.match(line);
    QRegularExpressionMatch matchN = rexn.match(line);
    QRegularExpressionMatch matchT = rext.match(line);
    if (match.hasMatch())//rexv.indexIn(line, 0) > -1)
    {
      Vector q = Vector(match.captured(1).toDouble(), match.captured(2).toDouble(), match.captured(3).toDouble()); vertices.push_back(q);
    }
    else if (matchN.hasMatch())//rexn.indexIn(line, 0) > -1)
    {
      Vector q = Vector(matchN.captured(1).toDouble(), matchN.captured(2).toDouble(), matchN.captured(3).toDouble());  normals.push_back(q);
    }
    else if (matchT.hasMatch())//rext.indexIn(line, 0) > -1)
    {
      varray.push_back(matchT.captured(1).toInt() - 1);
      varray.push_back(matchT.captured(3).toInt() - 1);
      varray.push_back(matchT.captured(5).toInt() - 1);
      narray.push_back(matchT.captured(2).toInt() - 1);
      narray.push_back(matchT.captured(4).toInt() - 1);
      narray.push_back(matchT.captured(6).toInt() - 1);
    }
  }
  data.close();
}

/*!
\brief Save the mesh in .obj format, with vertices and normals.
\param url Filename.
\param meshName %Mesh name in .obj file.
*/
void Mesh::SaveObj(const QString& url, const QString& meshName) const
{
  QFile data(url);
  if (!data.open(QFile::WriteOnly))
    return;
  QTextStream out(&data);
  out << "g " << meshName << Qt::endl;
  for (int i = 0; i < vertices.size(); i++)
    out << "v " << vertices.at(i)[0] << " " << vertices.at(i)[1] << " " << vertices.at(i)[2] << QString('\n');
  for (int i = 0; i < normals.size(); i++)
    out << "vn " << normals.at(i)[0] << " " << normals.at(i)[1] << " " << normals.at(i)[2] << QString('\n');
  for (int i = 0; i < varray.size(); i += 3)
  {
    out << "f " << varray.at(i) + 1 << "//" << narray.at(i) + 1 << " "
      << varray.at(i + 1) + 1 << "//" << narray.at(i + 1) + 1 << " "
      << varray.at(i + 2) + 1 << "//" << narray.at(i + 2) + 1 << " "
      << "\n";
  }
  out.flush();
  data.close();
}

/**
 * @brief Transform a mesh by multiplying vertices by a matrix
 * 
 * @param mat matrix to multiply by
 * @return Mesh& reference to the current mesh
 */
Mesh& Mesh::Transform(const Matrix & mat)
{
  for (int i = 0; i < vertices.size(); i++)
  {
    vertices[i] = mat * vertices[i];
  }
  for (int i = 0; i < normals.size(); i++)
  {
    normals[i] = mat * normals[i];
  }
  SmoothNormals();
  
  return (*this);
}

/**
 * @brief Rotate all vertices by an angle in the axis defined in parameters
 * 
 * @param axis Axis to rotate in
 * @param angle Angle in degree
 * @return Mesh& reference to the current mesh
 */
Mesh& Mesh::Rotate(const Vector & axis, double angle)
{
  return Transform(Matrix::Rotate(axis, angle));
}

/**
 * @brief Rotate the mesh in the X axis
 * 
 * @param angle Angle in degree
 * @return Mesh& Reference to the current mesh
 */
Mesh& Mesh::RotateX(double angle)
{
  return Transform(Matrix::RotateX(angle));
}

/**
 * @brief Rotate the mesh in the Y axis
 * 
 * @param angle Angle in degree
 * @return Mesh& Reference to the current mesh
 */
Mesh& Mesh::RotateY(double angle)
{
  return Transform(Matrix::RotateY(angle));
}

/**
 * @brief Rotate the mesh in the Z axis
 * 
 * @param angle Angle in degree
 * @return Mesh& Reference to the current mesh
 */
Mesh& Mesh::RotateZ(double angle)
{
  return Transform(Matrix::RotateZ(angle));
}

/**
 * @brief Scale the mesh 
 * 
 * @param scale How much the mesh should be scaled in each axis
 * @return Mesh& Reference to the current mesh
 */
Mesh& Mesh::Scale(const Vector & scale)
{
  return Transform(Matrix::Scale(scale));
}

/**
 * @brief Return the distance between two vector
 * 
 * @param u Vector 1
 * @param v Vector 2
 * @return double 
 */
double distanceV (const Vector& u, const Vector& v)
{
  double dX = v.getX() - u.getX();
  dX = dX * dX;

  double dY = v.getY() - u.getY();
  dY = dY * dY;

  double dZ = v.getZ() - u.getZ();
  dZ = dZ * dZ;

  return sqrt(dX + dY + dZ);
}

/**
 * @brief Increase or decrease height of neaby vector inside the sphere to match the vertex at the center of the sphere
 * 
 * @param sp Sphere to deform inside the mesh
 * @return Mesh& 
 */
Mesh & Mesh::Flattening(const Sphere & sp)
{

  Vector nearest = FindNearest(sp.getCenter());
  Sphere sphere(nearest, sp.getRadius());
  for (size_t i = 0; i < vertices.size(); i++)
  {
    vertices[i].z() += (sp.getCenter().getZ() - vertices[i].getZ()) * attenuationDistance(vertices[i], sphere);
  }
  SmoothNormals();
  return (*this);
}

/**
 * @brief Find the nearest vertex to a point inside a mesh
 * 
 * @param p point to search the closest vertice
 * @return Vector& 
 */
Vector & Mesh::FindNearest(const Vector & p)
{
  int index = 0;
  double actualDist = distanceV(vertices.at(0), p);
  for (size_t i = 1; i < this->vertices.size(); i++)
  {
    double dist = distanceV(vertices[i], p);
    if(dist < actualDist)
    {
      index = i;
      actualDist = dist;
    }
  }
  return vertices[index];
}

/**
 * @brief Merge two mesh together
 * 
 * @param mesh Mesh to merge with by adding each vertices, and id to the first one
 */
void Mesh::merge(const Mesh & mesh)
{
  int sV = mesh.varray.size();
  int sN = mesh.narray.size();
  for(int i = 0; i < sV; i++)
  {
    //std::cout<<i<<" s = "<< sV<<std::endl;
    //std::cout<<mesh.varray.at(i)<<std::endl;
    this->varray.push_back(mesh.varray.at(i) + this->vertices.size());
  }
  for(int i = 0; i < sN; i++)
  {
    //std::cout<<i<<" ns = "<< sN<<std::endl;
    //std::cout<<mesh.narray.at(i)<<std::endl;
    this->narray.push_back(mesh.narray.at(i) + this->normals.size());
  }

  for(int i = 0; i < mesh.vertices.size(); i++)
  {
    this->vertices.push_back(mesh.vertices.at(i));
  }
  for(int i = 0; i < mesh.normals.size(); i++)
  {
    this->normals.push_back(mesh.normals.at(i));
  }
  
}

/**
 * @brief Deform the mesh by a force inside a sphere
 * 
 * @param sphere Sphere to deform the mesh with
 * @param vec Force to apply inside the sphere
 * @return Mesh& 
 */
Mesh & Mesh::SphereWarp(const Sphere & sphere, const Vector & vec) {
  for (size_t i = 0; i < this->vertices.size(); i++)
  {
    vertices[i] += vec * attenuationDistance(vertices[i], sphere);
  }
  return (*this);
}

/**
 * @brief Compute the inverse squared distance between a point and the center of a sphere, round to 0
 *
 * @param vec Point to evaluate the attenuation
 * @param sphere Sphere to evaluate the distance to
 * @return double 
 */
double Mesh::attenuationDistance(const Vector & vec, const Sphere & sphere) {
  double xd = (sphere.getCenter().getX() - vec.getX());
  double yd = (sphere.getCenter().getY() - vec.getY());
  double zd = (sphere.getCenter().getZ() - vec.getZ());
  double d = sqrt(xd * xd + yd * yd + zd * zd) / sphere.getRadius();
  
  if(d > 1.0) return 0;
  else 
  {
    return (1.0 - d*d) * (1.0 - d*d);
  }

}

/**
 * @brief Add a sphere to each vertices
 * 
 * @param r Radius of the spheres
 * @return Mesh& 
 */
Mesh & Mesh::AddSphereToVertices(double r) {
  int size = vertices.size();
  for (size_t i = 0; i < size; i++)
  {
    this->merge(Mesh(Sphere(vertices[i], r), 15));
  }
  return (*this);
}

/**
 * @brief Add a box to each vertices
 * 
 * @param d Length of the box
 * @return Mesh& 
 */
Mesh & Mesh::AddBoxToVertices(double d) {
  int size = vertices.size();
  for (size_t i = 0; i < size; i++)
  {
    this->merge(Mesh(Box(d)).Translate(vertices[i]));
  }
  return (*this);
}

/**
 * @brief Add Boxes to each vertices multiple times with decreasing chance
 * 
 * @param d Length of the box
 * @param iteration Number of iteration to make
 * @param chance Chance to add the last iteration
 * @return Mesh& 
 */
Mesh & Mesh::AddBoxFractal(double d, int iteration, double chance) {
  int size = 0;
  double s = iteration - 1;
  for(int i = 0; i < iteration; i++) {
    Mesh temp;
    double c = 90 * ((s - i)/s) + chance * (i / s);
    for(int j = size; j < vertices.size(); j++) {
      if(rand()%100 <= c)temp.merge(Mesh(Box(d / (i + 1))).Translate(vertices[j]));
    }
    size = vertices.size() - 1;
    this->merge(temp);
  }
  return (*this);
}

/**
 * @brief Evaluate the normal of a given point inside a terrain
 * 
 * @param i line of the point
 * @param j column of the point
 * @param alt altitude factor
 * @param fn Noise generator
 * @return Normalized vector
 */
Vector terrainNormal(int i, int j, double alt, const FastNoise & fn)//Calcul la normals en i j
{
  Vector ab, cd;
  ab = Vector(i + 1, j, alt * (1 + fn.GetNoise(i + 1, j)) / 2.0) - Vector(i - 1, j, alt * (1 + fn.GetNoise(i - 1, j)) / 2.0);
  cd = Vector(i, j + 1, alt * (1 + fn.GetNoise(i, j + 1)) / 2.0) - Vector(i, j - 1, alt * (1 + fn.GetNoise(i, j - 1)) / 2.0);
  Normalize(ab);
  Normalize(cd);
  return ab/cd;
}

/**
 * @brief Construct a new Terrain Mesh object
 * 
 * @param terrain Terrain object with needed data
 * @param noise Noise generator to generate the heightmap
 */
Mesh::Mesh(const Terrain & terrain, const FastNoise & noise)
{
  unsigned int w = terrain.getWidth();
  unsigned int h = terrain.getHeight();
  Vector center = terrain.getCenter();
  for(int i = 0; i < h; i++)
  {
    for(int j = 0; j < w; j++)
    {
      double height = ((noise.GetNoise(j, i) + 1.0)/2.0)*terrain.getAltitude();//sqrt(pow(0 - ((j - w/2.0))/w , 2) + pow(0 - (i - h/2.0)/h, 2)) * terrain.getAltitude();
      vertices.push_back(Vector(j, i, height) - Vector(w/2.0, h/2.0, 0) + terrain.getCenter());
      normals.push_back(terrainNormal(j, i, terrain.getAltitude(), noise));
      if(i > 0 && j < w - 1) {
        AddSmoothTriangle(
          vertices.size() - 1, normals.size() - 1,
          vertices.size() - 1 - w, normals.size() - 1 - w,
          vertices.size() - w, normals.size() - w);
        AddSmoothTriangle(
          vertices.size() - 1, normals.size() - 1,
          vertices.size() - w, normals.size() - w,
          vertices.size(), normals.size());
      }
    }
  }
}

/**
 * @brief Colore a terrain by the slope
 * 
 * @param h height of the terrain
 * @param w width of the terrain
 * @param c1 color 1
 * @param c2 color 2
 * @param noise Noise generator
 * @return std::vector<Color> 
 */
std::vector<Color> Mesh::ColorationSlopeTerrain(int h, int w, Color c1, Color c2, FastNoise noise) {

	std::vector<Color> cols;
	cols.resize(Vertexes());

  for(int i = 0; i < h; i++)
  {
    for(int j = 0; j < w; j++)
    {
      Vector gradient = Vector(noise.GetNoise(j+1,i) - noise.GetNoise(j,i), noise.GetNoise(j,i+1) - noise.GetNoise(j,i), 0);
      double slope = Norm(gradient);

      cols[i*w+j] = Color::Lerp(slope*10, c1, c2);
    }
  }

    return cols;
}

/**
 * @brief Color a terrain by the laplacian method
 * 
* @param h height of the terrain
 * @param w width of the terrain
 * @param c1 color 1
 * @param c2 color 2
 * @param noise Noise generator
 * @return std::vector<Color> 
 */
std::vector<Color> Mesh::ColorationLaplaceTerrain(int h, int w, Color c1, Color c2, FastNoise noise) {

	std::vector<Color> cols;
	cols.resize(Vertexes());

  for(int i = 0; i < h; i++)
  {
    for(int j = 0; j < w; j++)
    {
      double laplace = noise.GetNoise(j-1,i) +  noise.GetNoise(j+1,i) + noise.GetNoise(j,i-1) + noise.GetNoise(j,i+1) - 4*noise.GetNoise(j,i);
      cols[i*w+j] = Color::Lerp(laplace*50, c1, c2);
    }
  }

    return cols;
}

/**
 * @brief Recalculate all noise values in the terrain
 * 
 * @param terrain Terrain object with needed data
 * @param noise Noise generator with updated parameters
 * @return Mesh& 
 */
Mesh& Mesh::UpdateNoise(const Terrain & terrain, const FastNoise & noise)
{
  unsigned int w = terrain.getWidth();
  unsigned int h = terrain.getHeight();
  Vector center = terrain.getCenter();
  for(int i = 0; i < h; i++)
  {
    for(int j = 0; j < w; j++)
    {
      double height = ((noise.GetNoise(j, i) + 1.0)/2.0)*terrain.getAltitude();
      vertices[i * w + j].z() = height + terrain.getCenter().getZ();
      normals[i * w + j] = (terrainNormal(j, i, terrain.getAltitude(), noise));
    }
  }
  return (*this);
}

/**
 * @brief Update all height by a factor inside a terrain
 * 
 * @param ratio ratio to multiply all vertices by
 * @return Mesh& 
 */
Mesh& Mesh::UpdateHeight(double ratio)
{
  for (size_t i = 0; i < vertices.size(); i++)
  {
    vertices[i].z() *= ratio;
  }
  SmoothNormals();
  return (*this);
}

/**
 * @brief Add Tetrahedre to all vertices, pointing to it's normal
 * 
 */
void Mesh::AddTetraToNormals() {
  Mesh m;
  for (int i=0; i<varray.size()/3; i++){
    Vector center = GetTriangle(i).Center() + GetTriangle(i).Normal();
    Tetraedre t(center, .25);
    m.merge(Mesh(t));
  }
  merge(m);
}