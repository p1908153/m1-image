#include "disk.h"

/**
 * @class Disk disk.h
 * @brief Disk volumic representation.
 */

/**
 * @brief Construct a new Disk object
 * 
 * @param c Center of the disk
 * @param r Radius of the disk
 */
Disk::Disk(const Vector& c, double r) : radius(r),  center(c) {}

/**
 * @brief Construct a new Disk object
 * 
 * @param c Center with an array
 * @param r radius of the disk
 */
Disk::Disk(const std::vector<double>& c, double r) : radius(r) {
    for (int i=0; i<3; i++){
        center[i] = c[i];
    }
}

/**
 * @brief Get the radius of the disk
 * 
 * @return double 
 */
double Disk::getRadius() const {
    return radius;
}

/**
 * @brief Get the center of the disk
 * 
 * @return Vector 
 */
Vector Disk::getCenter() const {
    return center;
}

/**
 * @brief Get a X Y Z point at the disk border with an angle
 * 
 * @param alpha angle in degree 
 * @return Vector who belongs to the disk border
 */
Vector Disk::parametrise(double alpha) const
{
    return Vector(sin(alpha), center.getY(), cos(alpha));
}