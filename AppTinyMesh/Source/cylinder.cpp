#include "cylinder.h"

/**
 * @class Cylinder cylinder.h
 * @brief Cylinder volumic representation.
 */

/**
 * @brief Construct a new Cylinder object with a radius and height of 1 and a center of (0, 0, 0)
 * 
 */
Cylinder::Cylinder() : radius(1.0), height(1.0), center(Vector(0.0,0.0,0.0)) {}

/**
 * @brief Construct a new Cylinder object with a center of(0, 0, 0)
 * 
 * @param r radius of the cylinder
 * @param h Height of the cylinder
 */
Cylinder::Cylinder(double r, double h) : radius(r), height(h), center(Vector(0.0,0.0,0.0)){}

/**
 * @brief Construct a new Cylinder object
 * 
 * @param c Center of the cylinder
 * @param r Radius of the cylinder
 * @param h Height of the cylinder
 */
Cylinder::Cylinder(const Vector& c, double r, double h) : radius(r), height(h), center(c) {}

/**
 * @brief Construct a new Cylinder object
 * 
 * @param c Center of the cylinder as a double array
 * @param r Radius of the cylinder
 * @param h height of the cylinder
 */
Cylinder::Cylinder(const std::vector<double>& c, double r, double h) : radius(r), height(h) {
    for (int i=0; i<3; i++){
        center[i] = c[i];
    }
}

/**
 * @brief Get the height of the cylinder
 * 
 * @return double 
 */
double Cylinder::getHeight() const {
    return height;
}

/**
 * @brief Get the radius of the cylinder
 * 
 * @return double 
 */
double Cylinder::getRadius() const {
    return radius;
}

/**
 * @brief Get the center of the cylinder
 * 
 * @return Vector 
 */
Vector Cylinder::getCenter() const {
    return center;
}

/**
 * @brief Get a point at a X Y Z coordinate with an angle and a height
 * 
 * @param alpha Angle in degree
 * @param y Height of the point
 * @return Vector which belong to the cylinder
 */
Vector Cylinder::parametrise(double alpha, double y) const
{
    return Vector(radius * cos(alpha), y, radius * sin(alpha));
}

