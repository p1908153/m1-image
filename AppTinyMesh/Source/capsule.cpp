#include "capsule.h"
/**
 * @class Capsule capsule.h
 * @brief Capsule volumic representation.
 */

/**
 * @brief Construct a new Capsule object
 * 
 * @param c Center of the capsule (Vector)
 * @param r Radius of the capsule
 * @param h Height of the capsule cylinder
 */
Capsule::Capsule(const Vector & c, double r, double h) : center(c), radius(r), height(h){}

/**
 * @brief Construct a new Capsule object with a center of (0, 0, 0)
 * 
 * @param r Radius of the capsule
 * @param h height of the capsule cylinder
 */
Capsule::Capsule(double r, double h) : center(Vector(0.0,0.0,0.0)), radius(r), height(h){}

/**
 * @brief Construct a new Capsule object
 * 
 * @param c Center of the capsule (Array of three int)
 * @param r Radius of the capsule
 * @param h Height of the capsule cylinder
 */
Capsule::Capsule(const std::vector<int> & c, double r, double h) : radius(r), height(h){
    for (int i=0; i<3; i++){
        center[i] = c[i];
    }
}

/**
 * @brief Get the radius of a Capsule
 * 
 * @return double 
 */
double Capsule::getRadius() const{
    return radius;
}

/**
 * @brief Get the height of a Capsule
 * 
 * @return double 
 */
double Capsule::getHeight() const
{
    return height;
}

/**
 * @brief Get the center of a capsule
 * 
 * @return Vector 
 */
Vector Capsule::getCenter() const
{
    return center;
}