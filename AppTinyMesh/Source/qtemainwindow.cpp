#include "qte.h"
#include "implicits.h"
#include "ui_interface.h"
int octave = 3;
int gain = 50;
int frequency = 10;
int lacunarity = 200;
int seed = 42;
int altitude = 100;
int precision = 90;

bool isTerrain = false;

Vector posWarp = Vector(0);
Vector sWarp = Vector(1);
double rayon = 1;

FastNoise noise;
Terrain t = Terrain(250, 250, altitude, Vector(0, 0, -50));
QString facteurCouleur = "Pente";
Color couleur1 = Color(0.5,1.0,0.0);
Color couleur2 = Color(1.0,0.0,0.0);

Mesh mesh;

/*!
\brief Creates the User's window.
*/
MainWindow::MainWindow() : QMainWindow(), uiw(new Ui::Assets)
{
	// Chargement de l'interface
    uiw->setupUi(this);

	// Chargement du GLWidget
	meshWidget = new MeshWidget;
	QGridLayout* GLlayout = new QGridLayout;
	GLlayout->addWidget(meshWidget, 0, 0);
	GLlayout->setContentsMargins(0, 0, 0, 0);
    uiw->widget_GL->setLayout(GLlayout);

	// Creation des connect
	CreateActions();

	meshWidget->SetCamera(Camera(Vector(0, -10, 0), Vector(0.0, 0.0, 0.0)));
}

/*!
\brief Deletes the User's window on the heap.
*/
MainWindow::~MainWindow()
{
	delete meshWidget;
}

/*!
\brief Links User's'actions on the Interface with associated functions.
*/
void MainWindow::CreateActions()
{
	// Buttons
    connect(uiw->boxMesh, SIGNAL(clicked()), this, SLOT(BoxMeshExample()));
    connect(uiw->tetraedreMesh, SIGNAL(clicked()), this, SLOT(TetraedreMeshExample()));
	connect(uiw->sphereImplicit, SIGNAL(clicked()), this, SLOT(SphereImplicitExample()));
	connect(uiw->sphereMesh, SIGNAL(clicked()), this, SLOT(SphereMeshExample()));
	connect(uiw->torMesh, SIGNAL(clicked()), this, SLOT(TorMeshExample()));
	connect(uiw->diskMesh, SIGNAL(clicked()), this, SLOT(DiskMeshExample()));
	connect(uiw->terrainMesh, SIGNAL(clicked()), this, SLOT(TerrainMeshExample()));
	connect(uiw->cylinderMesh, SIGNAL(clicked()), this, SLOT(CylinderMeshExample()));
	connect(uiw->capsuleMesh, SIGNAL(clicked()), this, SLOT(CapsuleMeshExample()));
	connect(uiw->boxFractal, SIGNAL(clicked()), this, SLOT(BoxFractalMeshExample()));
	connect(uiw->warpSifflet, SIGNAL(clicked()), this, SLOT(WarpSiffletMeshExample()));
	connect(uiw->snowman, SIGNAL(clicked()), this, SLOT(SnowManMeshExample()));
	connect(uiw->warpSiffletDebug, SIGNAL(clicked()), this, SLOT(WarpSiffletDebugMeshExample()));
	connect(uiw->resetcameraButton, SIGNAL(clicked()), this, SLOT(ResetCamera()));
	connect(uiw->wireframe, SIGNAL(clicked()), this, SLOT(UpdateMaterial()));

	connect(uiw->radioShadingButton_1, SIGNAL(clicked()), this, SLOT(UpdateMaterial()));
	connect(uiw->radioShadingButton_2, SIGNAL(clicked()), this, SLOT(UpdateMaterial()));
	connect(uiw->radioShadingButton_2, SIGNAL(clicked()), this, SLOT(UpdateMaterial()));

	connect(uiw->cubeToVertices, SIGNAL(clicked()), this, SLOT(BoxVerticesDebug()));
	connect(uiw->tetraToNormals, SIGNAL(clicked()), this, SLOT(TetraNormalsDebug()));

	connect(uiw->frequency, SIGNAL(valueChanged(int)), this, SLOT(setFrequency(int)));
	connect(uiw->frequency, SIGNAL(valueChanged(int)), uiw->frequencyV, SLOT(setValue(int)));
	connect(uiw->frequencyV, SIGNAL(valueChanged(int)), uiw->frequency, SLOT(setValue(int)));

	connect(uiw->precisionV, SIGNAL(valueChanged(int)), this, SLOT(setPrecision(int)));

	connect(uiw->lacunarity, SIGNAL(valueChanged(int)), this, SLOT(setLacunarity(int)));
	connect(uiw->lacunarity, SIGNAL(valueChanged(int)), uiw->lacunarityV, SLOT(setValue(int)));
	connect(uiw->lacunarityV, SIGNAL(valueChanged(int)), uiw->lacunarity, SLOT(setValue(int)));

	connect(uiw->octave, SIGNAL(valueChanged(int)), this, SLOT(setOctave(int)));
	connect(uiw->octave, SIGNAL(valueChanged(int)), uiw->octaveV, SLOT(setValue(int)));
	connect(uiw->octaveV, SIGNAL(valueChanged(int)), uiw->octave, SLOT(setValue(int)));

	connect(uiw->gain, SIGNAL(valueChanged(int)), this, SLOT(setGain(int)));
	connect(uiw->gain, SIGNAL(valueChanged(int)), uiw->gainV, SLOT(setValue(int)));
	connect(uiw->gainV, SIGNAL(valueChanged(int)), uiw->gain, SLOT(setValue(int)));

	connect(uiw->altitude, SIGNAL(valueChanged(int)), this, SLOT(setAltitude(int)));
	connect(uiw->altitude, SIGNAL(valueChanged(int)), uiw->altitudeV, SLOT(setValue(int)));
	connect(uiw->altitudeV, SIGNAL(valueChanged(int)), uiw->altitude, SLOT(setValue(int)));

	connect(uiw->pX, SIGNAL(valueChanged(double)), this, SLOT(setPx(double)));
	connect(uiw->pY, SIGNAL(valueChanged(double)), this, SLOT(setPy(double)));
	connect(uiw->pZ, SIGNAL(valueChanged(double)), this, SLOT(setPz(double)));

	connect(uiw->cX, SIGNAL(valueChanged(double)), this, SLOT(setCx(double)));
	connect(uiw->cY, SIGNAL(valueChanged(double)), this, SLOT(setCy(double)));
	connect(uiw->cZ, SIGNAL(valueChanged(double)), this, SLOT(setCz(double)));

	connect(uiw->rayon, SIGNAL(valueChanged(double)), this, SLOT(setRayon(double)));
	connect(uiw->sphereWarp, SIGNAL(clicked()), this, SLOT(applySphereWarp()));
	connect(uiw->flattening, SIGNAL(clicked()), this, SLOT(applyFlattening()));
	connect(uiw->translate, SIGNAL(clicked()), this, SLOT(Translate()));
	connect(uiw->scale, SIGNAL(clicked()), this, SLOT(Scale()));
	connect(uiw->addSphere, SIGNAL(clicked()), this, SLOT(addSphere()));

	connect(uiw->seed, SIGNAL(textChanged(const QString &)), this, SLOT(setSeed(QString)));

	
	connect(uiw->facteurCouleur, SIGNAL(currentTextChanged(const QString &)), this, SLOT(setFacteurCouleur(QString)));
	connect(uiw->couleur1, SIGNAL(currentTextChanged(const QString &)), this, SLOT(setCouleur1(QString)));
	connect(uiw->couleur2, SIGNAL(currentTextChanged(const QString &)), this, SLOT(setCouleur2(QString)));

	uiw->frequency->setValue(frequency);
	uiw->lacunarity->setValue(lacunarity);
	uiw->octave->setValue(octave);
	uiw->gain->setValue(gain);
	uiw->seed->setText("Binou");

	uiw->pX->setValue(posWarp.x());
	uiw->pY->setValue(posWarp.y());
	uiw->pZ->setValue(posWarp.z());

	uiw->cX->setValue(sWarp.x());
	uiw->cY->setValue(sWarp.y());
	uiw->cZ->setValue(sWarp.z());

	uiw->rayon->setValue(rayon);
	

	uiw->frequencyV->setValue(frequency);
	uiw->lacunarityV->setValue(lacunarity);
	uiw->octaveV->setValue(octave);
	uiw->gainV->setValue(gain);
	uiw->altitudeV->setValue(altitude);
	uiw->precisionV->setValue(precision);

	
	uiw->facteurCouleur->setPlaceholderText(facteurCouleur);


	// Widget edition
	connect(meshWidget, SIGNAL(_signalEditSceneLeft(const Ray&)), this, SLOT(editingSceneLeft(const Ray&)));
	connect(meshWidget, SIGNAL(_signalEditSceneRight(const Ray&)), this, SLOT(editingSceneRight(const Ray&)));
}

/*!
\brief Updates the field with a given frequency.
\param value The desired frequency.
*/
void MainWindow::setFrequency(int value) {
	
	auto start = std::chrono::high_resolution_clock::now(); // start timer

	frequency = value;
	if(isTerrain)
	{
		noise.SetFrequency(frequency/1000.0);
		mesh.UpdateNoise(t, noise);
		std::vector<Color> cols;
		cols.resize(mesh.Vertexes());
		if(facteurCouleur.toStdString() == "Pente") {
			cols = mesh.ColorationSlopeTerrain(t.getWidth(), t.getHeight(), couleur1, couleur2, noise);
		} else {
			cols = mesh.ColorationLaplaceTerrain(t.getWidth(), t.getHeight(), couleur1, couleur2, noise);
		}
		meshColor = MeshColor(mesh, cols, mesh.VertexIndexes());

		auto end = std::chrono::high_resolution_clock::now(); // end timer
		long long timer = (end - start).count() / 1000000.0; // Get the elapse time in milliseconds

   		uiw->elapseTime->setText(QString::number(timer));

		UpdateGeometry();
	}
}

/*!
\brief Applies a local deformation (SphereWrap) on the current mesh.
*/
void MainWindow::applySphereWarp()
{
	auto start = std::chrono::high_resolution_clock::now(); // start timer

	size_t size = mesh.Vertexes();
	mesh.SphereWarp(Sphere(posWarp, rayon), sWarp);
	std::vector<Color> cols;
	cols.resize(mesh.Vertexes());
    for (size_t i = 0; i < cols.size(); i++)
	{
		if(i < size) 
		{
			cols[i] = meshColor.GetColor(i);
		}
		else 
		{
			cols[i] = Color(1.0, 0.0, 0.0, 0.0);
		}
	}

	meshColor = MeshColor(mesh, cols, mesh.VertexIndexes());

	auto end = std::chrono::high_resolution_clock::now(); // end timer
	long long timer = (end - start).count() / 1000000.0; // Get the elapse time in milliseconds

   	uiw->elapseTime->setText(QString::number(timer));

	UpdateGeometry();
}

/*!
\brief Applies a local deformation (Flattening) on the current mesh.
*The location of the deformation is the global vector posWrap.
*The radius of the sphere used in the algorithm is the global variable rayon.
*/
void MainWindow::applyFlattening()
{
	auto start = std::chrono::high_resolution_clock::now(); // start timer

	size_t size = mesh.Vertexes();
	if(mesh.Vertexes() > 0) mesh.Flattening(Sphere(posWarp, rayon));
	std::vector<Color> cols;
	cols.resize(mesh.Vertexes());
	if(isTerrain)
	{
		if(facteurCouleur.toStdString() == "Pente") {
		cols = mesh.ColorationSlopeTerrain(t.getWidth(), t.getHeight(), couleur1, couleur2, noise);
		} else {
			cols = mesh.ColorationLaplaceTerrain(t.getWidth(), t.getHeight(), couleur1, couleur2, noise);
		}
	}
	else
	{
		for (size_t i = 0; i < cols.size(); i++)
		{
			if(i < size) 
			{
				cols[i] = meshColor.GetColor(i);
			}
			else 
			{
				cols[i] = Color(1.0, 0.0, 0.0, 0.0);
			}
		}
	}
	
	meshColor = MeshColor(mesh, cols, mesh.VertexIndexes());

	auto end = std::chrono::high_resolution_clock::now(); // end timer
	long long timer = (end - start).count() / 1000000.0; // Get the elapse time in milliseconds

   	uiw->elapseTime->setText(QString::number(timer));

	UpdateGeometry();
}

/*!
\brief Applies a translation on the current mesh, using global parameter sWrap as the translation matrix.
*/
void MainWindow::Translate()
{
	auto start = std::chrono::high_resolution_clock::now(); // start timer

	size_t size = mesh.Vertexes();
	mesh.Translate(sWarp);
	std::vector<Color> cols;
	cols.resize(mesh.Vertexes());
    for (size_t i = 0; i < cols.size(); i++)
	{
		if(i < size) 
		{
			cols[i] = meshColor.GetColor(i);
		}
		else 
		{
			cols[i] = Color(1.0, 0.0, 0.0, 0.0);
		}
	}

	meshColor = MeshColor(mesh, cols, mesh.VertexIndexes());

	auto end = std::chrono::high_resolution_clock::now(); // end timer
	long long timer = (end - start).count() / 1000000.0; // Get the elapse time in milliseconds

   	uiw->elapseTime->setText(QString::number(timer));

	UpdateGeometry();
}

/*!
\brief Resizes the current mesh, using global vector sWrap.
*/
void MainWindow::Scale()
{
	auto start = std::chrono::high_resolution_clock::now(); // start timer

	size_t size = mesh.Vertexes();
	mesh.Scale(sWarp);
	std::vector<Color> cols;
	cols.resize(mesh.Vertexes());
    for (size_t i = 0; i < cols.size(); i++)
	{
		if(i < size) 
		{
			cols[i] = meshColor.GetColor(i);
		}
		else 
		{
			cols[i] = Color(1.0, 0.0, 0.0, 0.0);
		}
	}

	meshColor = MeshColor(mesh, cols, mesh.VertexIndexes());

	auto end = std::chrono::high_resolution_clock::now(); // end timer
	long long timer = (end - start).count() / 1000000.0; // Get the elapse time in milliseconds

   	uiw->elapseTime->setText(QString::number(timer));

	UpdateGeometry();
}

/*!
\brief Merges the current mesh with a sphere using the global vector posWarp as the sphere's center
*and the global variable rayon as the radius.
*/
void MainWindow::addSphere()
{
	auto start = std::chrono::high_resolution_clock::now(); // start timer

	size_t size = mesh.Vertexes();
	mesh.merge(Mesh(Sphere(posWarp, rayon)));
	
	std::vector<Color> cols;
	cols.resize(mesh.Vertexes());
    for (size_t i = 0; i < cols.size(); i++)
	{
		if(i < size) 
		{
			cols[i] = meshColor.GetColor(i);
		}
		else 
		{
			cols[i] = Color(1.0, 0.0, 0.0, 0.0);
		}
	}

	meshColor = MeshColor(mesh, cols, mesh.VertexIndexes());

	auto end = std::chrono::high_resolution_clock::now(); // end timer
	long long timer = (end - start).count() / 1000000.0; // Get the elapse time in milliseconds

   	uiw->elapseTime->setText(QString::number(timer));

	UpdateGeometry();
}

/*!
\brief Sets the value of the global vector posWarp's position on the x axis.
\param value New value of the global vector posWarp's position on the x axis.
*/
void MainWindow::setPx(double value) {
	posWarp.x() = value;
	// std::cout<<"Pos = "<<posWarp<<std::endl;
}

/*!
\brief Sets the value of the global vector posWarp's position on the y axis.
\param value New value of the global vector posWarp's position on the y axis.
*/
void MainWindow::setPy(double value) {
	posWarp.y() = value;
	// std::cout<<"Pos = "<<posWarp<<std::endl;
}

/*!
\brief Sets the value of the global vector posWarp's position on the z axis.
\param value New value of the global vector posWarp's position on the z axis.
*/
void MainWindow::setPz(double value) {
	posWarp.z() = value;
	// std::cout<<"Pos = "<<posWarp<<std::endl;
}

/*!
\brief Sets the value of the global vector sWarp's position on the x axis.
\param value New value of the global vector sWarp's position on the x axis.
*/
void MainWindow::setCx(double value) {
	sWarp.x() = value;
	// std::cout<<"Force = "<<posWarp<<std::endl;
}

/*!
\brief Sets the value of the global vector sWarp's position on the y axis.
\param value New value of the global vector sWarp's position on the y axis.
*/
void MainWindow::setCy(double value) {
	sWarp.y() = value;
	// std::cout<<"Force = "<<posWarp<<std::endl;
}

/*!
\brief Sets the value of the global vector sWarp's position on the z axis.
\param value New value of the global vector sWarp's position on the z axis.
*/
void MainWindow::setCz(double value) {
	sWarp.z() = value;
	// std::cout<<"Force = "<<posWarp<<std::endl;
}

/*!
\brief Sets the value of the global variable rayon.
\param value New value of the global variable rayon.
*/
void MainWindow::setRayon(double value) {
	rayon = value;
	// std::cout<<"r = "<<value<<std::endl;
}

/*!
\brief Sets the value of the global variable precision.
\param value New value of the global variable precision.
*/
void MainWindow::setPrecision(int value) {
	precision = value;
}

/*!
\brief Updates the field with a given altitude.
\param value The desired altitude.
*/
void MainWindow::setAltitude(int value) {
	
	auto start = std::chrono::high_resolution_clock::now(); // start timer

	double ratio = (double)value / (double)altitude;
	altitude = value;
	t.setAltitude(altitude);
	if(isTerrain)
	{
		mesh.UpdateHeight(ratio);
		std::vector<Color> cols;
		cols.resize(mesh.Vertexes());
		if(facteurCouleur.toStdString() == "Pente") {
			cols = mesh.ColorationSlopeTerrain(t.getWidth(), t.getHeight(), couleur1, couleur2, noise);
		} else {
			cols = mesh.ColorationLaplaceTerrain(t.getWidth(), t.getHeight(), couleur1, couleur2, noise);
		}
		meshColor = MeshColor(mesh, cols, mesh.VertexIndexes());

		auto end = std::chrono::high_resolution_clock::now(); // end timer
		long long timer = (end - start).count() / 1000000.0; // Get the elapse time in milliseconds

   		uiw->elapseTime->setText(QString::number(timer));

		UpdateGeometry();
		
	}
}

/*!
\brief Updates the field with a given octave count.
\param value The desired octave count.
*/
void MainWindow::setOctave(int value)  {

	auto start = std::chrono::high_resolution_clock::now(); // start timer

	octave = value;
	if(isTerrain)
	{
		noise.SetFractalOctaves(octave);
		mesh.UpdateNoise(t, noise);
		std::vector<Color> cols;
		cols.resize(mesh.Vertexes());
		if(facteurCouleur.toStdString() == "Pente") {
			cols = mesh.ColorationSlopeTerrain(t.getWidth(), t.getHeight(), couleur1, couleur2, noise);
		} else {
			cols = mesh.ColorationLaplaceTerrain(t.getWidth(), t.getHeight(), couleur1, couleur2, noise);
		}
		meshColor = MeshColor(mesh, cols, mesh.VertexIndexes());

		auto end = std::chrono::high_resolution_clock::now(); // end timer
		long long timer = (end - start).count() / 1000000.0; // Get the elapse time in milliseconds

   		uiw->elapseTime->setText(QString::number(timer));

		UpdateGeometry();
	}
}

/*!
\brief Updates the field with a given octave gain.
\param value The desired octave gain.
*/
void MainWindow::setGain(int value)  {

	auto start = std::chrono::high_resolution_clock::now(); // start timer

	gain = value;
	if(isTerrain)
	{
		noise.SetFractalGain(gain/100.0);
		mesh.UpdateNoise(t, noise);
		std::vector<Color> cols;
		cols.resize(mesh.Vertexes());
		if(facteurCouleur.toStdString() == "Pente") {
			cols = mesh.ColorationSlopeTerrain(t.getWidth(), t.getHeight(), couleur1, couleur2, noise);
		} else {
			cols = mesh.ColorationLaplaceTerrain(t.getWidth(), t.getHeight(), couleur1, couleur2, noise);
		}
		meshColor = MeshColor(mesh, cols, mesh.VertexIndexes());

		auto end = std::chrono::high_resolution_clock::now(); // end timer
		long long timer = (end - start).count() / 1000000.0; // Get the elapse time in milliseconds

   		uiw->elapseTime->setText(QString::number(timer));

		UpdateGeometry();
	}
}

/*!
\brief Updates the field with a given octave lacunarity.
\param value The desired octave lacunarity.
*/
void MainWindow::setLacunarity(int value)  {
	
	auto start = std::chrono::high_resolution_clock::now(); // start timer

	lacunarity = value;
	if(isTerrain)
	{
		noise.SetFractalLacunarity(lacunarity/100.0);
		mesh.UpdateNoise(t, noise);
		std::vector<Color> cols;
		cols.resize(mesh.Vertexes());
		if(facteurCouleur.toStdString() == "Pente") {
			cols = mesh.ColorationSlopeTerrain(t.getWidth(), t.getHeight(), couleur1, couleur2, noise);
		} else {
			cols = mesh.ColorationLaplaceTerrain(t.getWidth(), t.getHeight(), couleur1, couleur2, noise);
		}
		meshColor = MeshColor(mesh, cols, mesh.VertexIndexes());

		auto end = std::chrono::high_resolution_clock::now(); // end timer
		long long timer = (end - start).count() / 1000000.0; // Get the elapse time in milliseconds

   		uiw->elapseTime->setText(QString::number(timer));

		UpdateGeometry();
	}
}

/*!
\brief Updates the field with a given seed.
\param value The desired seed.
*/
void MainWindow::setSeed(QString value) {
	
	auto start = std::chrono::high_resolution_clock::now(); // start timer

	seed = 0;
	for(int i = 0; i < value.length(); i++)
	{
		seed += value.at(i).unicode();
	}

	if(isTerrain)
	{
		noise.SetSeed(seed);
		mesh.UpdateNoise(t, noise);
		std::vector<Color> cols;
		cols.resize(mesh.Vertexes());
		if(facteurCouleur.toStdString() == "Pente") {
			cols = mesh.ColorationSlopeTerrain(t.getWidth(), t.getHeight(), couleur1, couleur2, noise);
		} else {
			cols = mesh.ColorationLaplaceTerrain(t.getWidth(), t.getHeight(), couleur1, couleur2, noise);
		}
		meshColor = MeshColor(mesh, cols, mesh.VertexIndexes());

		auto end = std::chrono::high_resolution_clock::now(); // end timer
		long long timer = (end - start).count() / 1000000.0; // Get the elapse time in milliseconds

   		uiw->elapseTime->setText(QString::number(timer));

		UpdateGeometry();
	}
}

/*!
\brief Updates the field with a given way to colorize it (by slope or by laplace operator).
\param value String naming the factor for the color.
*/
void MainWindow::setFacteurCouleur(QString value)  {
	
	auto start = std::chrono::high_resolution_clock::now(); // start timer

	facteurCouleur = value;
	if(isTerrain)
	{
		std::vector<Color> cols;
		cols.resize(mesh.Vertexes());
		if(facteurCouleur.toStdString() == "Pente") {
			cols = mesh.ColorationSlopeTerrain(t.getWidth(), t.getHeight(), couleur1, couleur2, noise);
		} else {
			cols = mesh.ColorationLaplaceTerrain(t.getWidth(), t.getHeight(), couleur1, couleur2, noise);
		}
		meshColor = MeshColor(mesh, cols, mesh.VertexIndexes());

		auto end = std::chrono::high_resolution_clock::now(); // end timer
		long long timer = (end - start).count() / 1000000.0; // Get the elapse time in milliseconds

   		uiw->elapseTime->setText(QString::number(timer));

		UpdateGeometry();
	}
}

/*!
\brief Updates the field with a given color.
\param value String naming the color.
*/
void MainWindow::setCouleur1(QString qvalue)  {
	
	auto start = std::chrono::high_resolution_clock::now(); // start timer

	std::string value = qvalue.toStdString();
	if(value == "rouge") couleur1 = Color(1.0,0.0,0.0);
	else if(value == "vert") couleur1 = Color(0.0,1.0,0.0);
	else if(value == "bleu") couleur1 = Color(0.0,0.0,1.0);
	else if(value == "jaune") couleur1 = Color(1.0,1.0,0.0);
	else if(value == "violet")couleur1 = Color(1.0,0.0,1.0);
	else if(value == "turquoise") couleur1 = Color(0.0,1.0,1.0);
	else if(value == "blanc") couleur1 = Color(1.0,1.0,1.0);
	else if(value == "noir") couleur1 = Color(0.0,0.0,0.0);
	if(isTerrain)
	{
		std::vector<Color> cols;
		cols.resize(mesh.Vertexes());
		if(facteurCouleur.toStdString() == "Pente") {
			cols = mesh.ColorationSlopeTerrain(t.getWidth(), t.getHeight(), couleur1, couleur2, noise);
		} else {
			cols = mesh.ColorationLaplaceTerrain(t.getWidth(), t.getHeight(), couleur1, couleur2, noise);
		}
		meshColor = MeshColor(mesh, cols, mesh.VertexIndexes());

		auto end = std::chrono::high_resolution_clock::now(); // end timer
		long long timer = (end - start).count() / 1000000.0; // Get the elapse time in milliseconds

   		uiw->elapseTime->setText(QString::number(timer));

		UpdateGeometry();
	}
}

/*!
\brief Updates the field with a given color.
\param value String naming the color.
*/
void MainWindow::setCouleur2(QString qvalue)  {
	
	auto start = std::chrono::high_resolution_clock::now(); // start timer

	std::string value = qvalue.toStdString();
	if(value == "rouge") couleur2 = Color(1.0,0.0,0.0);
	else if(value == "vert") couleur2 = Color(0.0,1.0,0.0);
	else if(value == "bleu") couleur2 = Color(0.0,0.0,1.0);
	else if(value == "jaune") couleur2 = Color(1.0,1.0,0.0);
	else if(value == "violet") couleur2 = Color(1.0,0.0,1.0);
	else if(value == "turquoise") couleur2 = Color(0.0,1.0,1.0);
	else if(value == "blanc") couleur2 = Color(1.0,1.0,1.0);
	else if(value == "noir") couleur2 = Color(0.0,0.0,0.0);
	if(isTerrain)
	{
		std::vector<Color> cols;
		cols.resize(mesh.Vertexes());
		if(facteurCouleur.toStdString() == "Pente") {
			cols = mesh.ColorationSlopeTerrain(t.getWidth(), t.getHeight(), couleur1, couleur2, noise);
		} else {
			cols = mesh.ColorationLaplaceTerrain(t.getWidth(), t.getHeight(), couleur1, couleur2, noise);
		}
		meshColor = MeshColor(mesh, cols, mesh.VertexIndexes());

		auto end = std::chrono::high_resolution_clock::now(); // end timer
		long long timer = (end - start).count() / 1000000.0; // Get the elapse time in milliseconds

   		uiw->elapseTime->setText(QString::number(timer));

		UpdateGeometry();
	}
}

void MainWindow::editingSceneLeft(const Ray&)
{
}

void MainWindow::editingSceneRight(const Ray&)
{
}


/*!
\brief Generates and displays the mesh of a field.
*/
void MainWindow::TerrainMeshExample()
{
	auto start = std::chrono::high_resolution_clock::now(); // start timer

	t = Terrain(250, 250, altitude, Vector(0, 0, -50));
	noise.SetNoiseType(FastNoise::NoiseType::SimplexFractal);
	noise.SetFractalOctaves(octave);
	noise.SetFractalGain(gain/100.0);
	noise.SetFractalLacunarity(lacunarity/100.0);
	noise.SetFrequency(frequency/1000.0);
	noise.SetSeed(seed);
	mesh = Mesh(t, noise);
	isTerrain = true;
	
	std::vector<Color> cols;

	if(facteurCouleur.toStdString() == "Pente") {
		cols = mesh.ColorationSlopeTerrain(t.getWidth(), t.getHeight(), couleur1, couleur2, noise);
	} else {
		cols = mesh.ColorationLaplaceTerrain(t.getWidth(), t.getHeight(), couleur1, couleur2, noise);
	}

	meshColor = MeshColor(mesh, cols, mesh.VertexIndexes());

	auto end = std::chrono::high_resolution_clock::now(); // end timer
	long long timer = (end - start).count() / 1000000.0; // Get the elapse time in milliseconds

    uiw->elapseTime->setText(QString::number(timer));

	UpdateGeometry();

}

/*!
\brief Generates and displays the mesh of a whistle.
*/
void MainWindow::WarpSiffletMeshExample()
{
	auto start = std::chrono::high_resolution_clock::now(); // start timer

	mesh = Mesh(Capsule(Vector(0, 0, 0), 1.0, 2.0), precision);
	isTerrain = false;
	
	mesh.SphereWarp(Sphere(Vector(0.0, 0.0, 1.0), 1.5), Vector(10, 0, 10));

	//boxMesh.merge(Mesh(Sphere(Vector(0.0, 0.0, 1.0), 1.5)));
	std::vector<Color> cols;
	cols.resize(mesh.Vertexes());
    for (size_t i = 0; i < cols.size(); i++)
	{
		cols[i] = Color((double)(rand()%100)/100.0, (double)(rand()%100)/100.0, (double)(rand()%100)/100.0, 0.0);
	}

	meshColor = MeshColor(mesh, cols, mesh.VertexIndexes());
	
	auto end = std::chrono::high_resolution_clock::now(); // end timer
	long long timer = (end - start).count() / 1000000.0; // Get the elapse time in milliseconds

    uiw->elapseTime->setText(QString::number(timer));
	
	UpdateGeometry();
}

/*!
\brief Generates and displays the mesh of a cap merged with a sphere used to generate the whistle.
*Used for debugging the whistle.
*/
void MainWindow::WarpSiffletDebugMeshExample()
{
	auto start = std::chrono::high_resolution_clock::now(); // start timer

	mesh = Mesh(Capsule(Vector(0, 0, 0), 1.0, 2.0), precision);
	isTerrain = false;
	
	//mesh.SphereWarp(Sphere(Vector(0.0, 0.0, 1.0), 1.5), Vector(10, 0, 10));
	mesh.merge(Mesh(Sphere(Vector(0.0, 0.0, 1.0), 1.5), precision));
	std::vector<Color> cols;
	cols.resize(mesh.Vertexes());
    for (size_t i = 0; i < cols.size(); i++)
		cols[i] = Color(double(i) / 6.0, fmod(double(i) * 39.478378, 1.0), 0.0);

	meshColor = MeshColor(mesh, cols, mesh.VertexIndexes());
	
	auto end = std::chrono::high_resolution_clock::now(); // end timer
	long long timer = (end - start).count() / 1000000.0; // Get the elapse time in milliseconds

    uiw->elapseTime->setText(QString::number(timer));

	UpdateGeometry();
}

/*!
\brief Generates and displays the mesh of a box.
*/
void MainWindow::BoxMeshExample()
{
	auto start = std::chrono::high_resolution_clock::now(); // start timer

	mesh = Mesh(Box(2.0));
	isTerrain = false;

	std::vector<Color> cols;
	cols.resize(mesh.Vertexes());
	
    for (size_t i = 0; i < cols.size(); i++)
		cols[i] = Color(double(i) / 6.0, fmod(double(i) * 39.478378, 1.0), 0.0);

	meshColor = MeshColor(mesh, cols, mesh.VertexIndexes());
	
	auto end = std::chrono::high_resolution_clock::now(); // end timer
	long long timer = (end - start).count() / 1000000.0; // Get the elapse time in milliseconds

    uiw->elapseTime->setText(QString::number(timer));

	UpdateGeometry();
}

/*!
\brief Generates and displays the mesh of a structure similar to a fractal.
*/
void MainWindow::BoxFractalMeshExample()
{
	auto start = std::chrono::high_resolution_clock::now(); // start timer

	mesh = Mesh(Box(8.0));
	isTerrain = false;
	mesh.AddBoxFractal(4, 6, 5);

	std::vector<Color> cols;
	cols.resize(mesh.Vertexes());
    for (size_t i = 0; i < cols.size(); i++)
		cols[i] = Color((double)(i+1)/cols.size(), (double)(i+1)/cols.size(), (double)(i+1)/cols.size(), 0.0);

	meshColor = MeshColor(mesh, cols, mesh.VertexIndexes());
	
	auto end = std::chrono::high_resolution_clock::now(); // end timer
	long long timer = (end - start).count() / 1000000.0; // Get the elapse time in milliseconds

    uiw->elapseTime->setText(QString::number(timer));

	UpdateGeometry();
}

/*!
\brief Generates and displays the mesh of a tetrahedron.
*/
void MainWindow::TetraedreMeshExample()
{
	auto start = std::chrono::high_resolution_clock::now(); // start timer

	mesh = Mesh(Tetraedre());
	isTerrain = false;

	std::vector<Color> cols;
	cols.resize(mesh.Vertexes());
    for (size_t i = 0; i < cols.size(); i++)
		cols[i] = Color(double(i) / 6.0, fmod(double(i) * 39.478378, 1.0), 0.0);

	meshColor = MeshColor(mesh, cols, mesh.VertexIndexes());

	auto end = std::chrono::high_resolution_clock::now(); // end timer
	long long timer = (end - start).count() / 1000000.0; // Get the elapse time in milliseconds

    uiw->elapseTime->setText(QString::number(timer));

	UpdateGeometry();
}

/*!
\brief Generates and displays the mesh of a sphere created implicitly.
*/
void MainWindow::SphereImplicitExample()
{
	auto start = std::chrono::high_resolution_clock::now(); // start timer

  	AnalyticScalarField implicit;

  	mesh;
  	implicit.Polygonize(31, mesh, Box(2.0));

  	std::vector<Color> cols;
  	cols.resize(mesh.Vertexes());
  	for (size_t i = 0; i < cols.size(); i++)
   		cols[i] = Color(0.8, 0.8, 0.8);

  	meshColor = MeshColor(mesh, cols, mesh.VertexIndexes());

	auto end = std::chrono::high_resolution_clock::now(); // end timer
	long long timer = (end - start).count() / 1000000.0; // Get the elapse time in milliseconds

    uiw->elapseTime->setText(QString::number(timer));

  	UpdateGeometry();
}

/*!
\brief Generates and displays the mesh of a sphere.
*/
void MainWindow::SphereMeshExample()
{	
	auto start = std::chrono::high_resolution_clock::now(); // start timer

    mesh = Mesh(Sphere(Vector(0, 0, 0), 4.0), precision);
	isTerrain = false;

	std::vector<Color> cols;
	cols.resize(mesh.Vertexes());
	for (int i = 0; i < cols.size(); i++)
        cols[i] = Color(0.8, 0.8, 0.8);

	meshColor = MeshColor(mesh, cols, mesh.VertexIndexes());
	
	auto end = std::chrono::high_resolution_clock::now(); // end timer
	long long timer = (end - start).count() / 1000000.0; // Get the elapse time in milliseconds

    uiw->elapseTime->setText(QString::number(timer));

    UpdateGeometry();
}

/*!
\brief Generates and displays the mesh of a snowman.
*/
void MainWindow::SnowManMeshExample()
{
	auto start = std::chrono::high_resolution_clock::now(); // start timer

    mesh = Mesh();
	
	// Tete
	mesh.merge(Mesh(Sphere(Vector(0, 0, 1), 0.5), precision));
	std::vector<Color> cols;
	cols.resize(mesh.Vertexes());
	for (int i = 0; i < cols.size(); i++)
        cols[i] = Color(1.0, 1.0, 1.0);
	
	int size = cols.size();
	mesh.merge(Mesh(Sphere(Vector(0, -0.15, 1.2), 0.2))
					.SphereWarp(Sphere(Vector(0, -0.4, 1.2), 0.1), Vector(0, -0.75, 0)));
	
	cols.resize(mesh.Vertexes());
	for (int i = size; i < cols.size(); i++)
        cols[i] = Color(1.0, 0.6, 0.1);
	
	size = cols.size();


	mesh.merge(Mesh(Sphere(Vector(-0.15, -0.35, 1.3), 0.05), precision));
	mesh.merge(Mesh(Sphere(Vector(0.15, -0.35, 1.3), 0.05), precision));
	cols.resize(mesh.Vertexes());
	for (int i = size; i < cols.size(); i++)
        cols[i] = Color(0.0, 0.0, 0.0);
	
	size = mesh.Vertexes();

	// CORPS
	mesh.merge(Mesh(Sphere(Vector(0, 0, 0), 0.75), precision));
	mesh.merge(Mesh(Sphere(Vector(0, 0, -1), 1), precision));

	cols.resize(mesh.Vertexes());
	for (int i = size; i < cols.size(); i++)
        cols[i] = Color(1.0, 1.0, 1.0);
	
	size = mesh.Vertexes();

	mesh.merge(Mesh(Capsule(Vector(0, 0, 0), 0.1, 1), precision)
				.Rotate(Vector(1, 0, -0.5), 90.0)
				.Translate(Vector(0.75, 0, 0.5)));

	mesh.merge(Mesh(Capsule(Vector(0, 0, 0), 0.1, 1), precision)
				.Rotate(Vector(1, 0, 0.5), 90.0)
				.Translate(Vector(-0.75, 0, 0.5)));

	cols.resize(mesh.Vertexes());
	for (int i = size; i < cols.size(); i++)
        cols[i] = Color(0.3, 0.25, 0.05);
	
	size = mesh.Vertexes();

	Mesh sifflet = Mesh(Capsule(Vector(0, 0, 0), 1.0, 2.0), precision);
	sifflet.SphereWarp(Sphere(Vector(0.0, 0.0, 1.0), 1.5), Vector(4, 0, 4));
	mesh.merge(sifflet.Scale(Vector(0.125, 0.125, 0.125)).RotateZ(90).RotateX(-45).Translate(Vector(0, -.7, 0.9)));


	
	isTerrain = false;

	cols.resize(mesh.Vertexes());
	for (int i = size; i < cols.size(); i++)
        cols[i] = Color(1.0, 0.0, 0.0);

	meshColor = MeshColor(mesh, cols, mesh.VertexIndexes());
	
	auto end = std::chrono::high_resolution_clock::now(); // end timer
	long long timer = (end - start).count() / 1000000.0; // Get the elapse time in milliseconds

    uiw->elapseTime->setText(QString::number(timer));

    UpdateGeometry();
}

/*!
\brief Generates and displays the mesh of a cylinder.
*/
void MainWindow::CylinderMeshExample()
{	
	auto start = std::chrono::high_resolution_clock::now(); // start timer

    mesh = Mesh(Cylinder(Vector(0, 0, 0), 1.0,2.0), precision);
	isTerrain = false;

	std::vector<Color> cols;
	cols.resize(mesh.Vertexes());
	for (int i = 0; i < cols.size(); i++)
        cols[i] = Color(0.8, 0.8, 0.8);

	meshColor = MeshColor(mesh, cols, mesh.VertexIndexes());
	meshColor = MeshColor(mesh, cols, mesh.VertexIndexes());
	
	auto end = std::chrono::high_resolution_clock::now(); // end timer
	long long timer = (end - start).count() / 1000000.0; // Get the elapse time in milliseconds

    uiw->elapseTime->setText(QString::number(timer));

    UpdateGeometry();
}

/*!
\brief Generates and displays the mesh of a torus.
*/
void MainWindow::TorMeshExample()
{
	auto start = std::chrono::high_resolution_clock::now(); // start timer

    mesh = Mesh(Tor(Vector(0, 0, 0), 2.0, 1.0), precision);
	isTerrain = false;

	std::vector<Color> cols;
	cols.resize(mesh.Vertexes());
	for (int i = 0; i < cols.size(); i++)
        cols[i] = Color(0.8, 0.8, 0.8);

	meshColor = MeshColor(mesh, cols, mesh.VertexIndexes());
	
	auto end = std::chrono::high_resolution_clock::now(); // end timer
	long long timer = (end - start).count() / 1000000.0; // Get the elapse time in milliseconds

    uiw->elapseTime->setText(QString::number(timer));

    UpdateGeometry();
}

/*!
\brief Generates and displays the mesh of a cap.
*/
void MainWindow::CapsuleMeshExample()
{
	auto start = std::chrono::high_resolution_clock::now(); // start timer

    mesh = Mesh(Capsule(Vector(0, 0, 0), 1.0, 2.0), precision);
	isTerrain = false;

	std::vector<Color> cols;
	cols.resize(mesh.Vertexes());
	for (int i = 0; i < cols.size(); i++)
        cols[i] = Color(0.8, 0.8, 0.8);

	meshColor = MeshColor(mesh, cols, mesh.VertexIndexes());
	
	auto end = std::chrono::high_resolution_clock::now(); // end timer
	long long timer = (end - start).count() / 1000000.0; // Get the elapse time in milliseconds

    uiw->elapseTime->setText(QString::number(timer));

    UpdateGeometry();
}

/*!
\brief Generates and displays the mesh of a disk.
*/
void MainWindow::DiskMeshExample()
{
	auto start = std::chrono::high_resolution_clock::now(); // start timer

	mesh = Mesh(Disk(Vector(0, 0, 0), 1.0), precision);
	isTerrain = false;

	std::vector<Color> cols;
	cols.resize(mesh.Vertexes());
	for (int i = 0; i < cols.size(); i++)
        cols[i] = Color(0.8, 0.8, 0.8);

	meshColor = MeshColor(mesh, cols, mesh.VertexIndexes());
	
	auto end = std::chrono::high_resolution_clock::now(); // end timer
	long long timer = (end - start).count() / 1000000.0; // Get the elapse time in milliseconds

    uiw->elapseTime->setText(QString::number(timer));

    UpdateGeometry();
}

/*!
\brief Adds boxes to every vertice of the current mesh.
*/
void MainWindow::BoxVerticesDebug()
{
	auto start = std::chrono::high_resolution_clock::now(); // start timer

	size_t size = mesh.Vertexes();
	mesh.AddBoxToVertices(0.05);

	std::vector<Color> cols;
	cols.resize(mesh.Vertexes());
    for (size_t i = 0; i < cols.size(); i++)
	{
		if(i < size) 
		{
			//std::cout<<meshColor.Vertex(i)<<std::endl;
			cols[i] = meshColor.GetColor(i);
		}
		else 
		{
			cols[i] = Color(1.0, 0.0, 0.0, 0.0);
		}
	}
		

	meshColor = MeshColor(mesh, cols, mesh.VertexIndexes());
	
	auto end = std::chrono::high_resolution_clock::now(); // end timer
	long long timer = (end - start).count() / 1000000.0; // Get the elapse time in milliseconds

    uiw->elapseTime->setText(QString::number(timer));

	UpdateGeometry();
}

/*!
\brief Adds tetrahedrons in the direction of every triangle normal on the current mesh.
*/
void MainWindow::TetraNormalsDebug()
{
	auto start = std::chrono::high_resolution_clock::now(); // start timer

	size_t size = mesh.Vertexes();
	mesh.AddTetraToNormals();

	std::vector<Color> cols;
	cols.resize(mesh.Vertexes());
    for (size_t i = 0; i < cols.size(); i++)
	{
		if(i < size) 
		{
			//std::cout<<meshColor.Vertex(i)<<std::endl;
			cols[i] = meshColor.GetColor(i);
		}
		else 
		{
			cols[i] = Color(1.0, 0.0, 0.0, 0.0);
		}
	}
		

	meshColor = MeshColor(mesh, cols, mesh.VertexIndexes());
	
	auto end = std::chrono::high_resolution_clock::now(); // end timer
	long long timer = (end - start).count() / 1000000.0; // Get the elapse time in milliseconds

    uiw->elapseTime->setText(QString::number(timer));

	UpdateGeometry();
}

/*!
\brief Generates an image on the User's Interface and the statistics of the image.
*/
void MainWindow::UpdateGeometry()
{
	meshWidget->ClearAll();
	meshWidget->AddMesh("BoxMesh", meshColor);

    uiw->lineEdit->setText(QString::number(meshColor.Vertexes()));
    uiw->lineEdit_2->setText(QString::number(meshColor.Triangles()));

	UpdateMaterial();
}

/*!
\brief Generates an image on the User's Interface.
*/
void MainWindow::UpdateMaterial()
{
    meshWidget->UseWireframeGlobal(uiw->wireframe->isChecked());

    if (uiw->radioShadingButton_1->isChecked())
		meshWidget->SetMaterialGlobal(MeshMaterial::Normal);
	else
		meshWidget->SetMaterialGlobal(MeshMaterial::Color);
}

/*!
\brief Sets the camera on a default value on the User's Interface.
*/
void MainWindow::ResetCamera()
{
	meshWidget->SetCamera(Camera(Vector(-10.0), Vector(0.0)));
}
