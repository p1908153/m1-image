#include "terrain.h"
/**
 * @class Terrain terrain.h
 * @brief Terrain volumic representation.
 */


/// @brief Construct a new terrain object.
/// @param w Height of the field.
/// @param h Width of the field.
/// @param a Max altitude of the field.
/// @param center Center of the field.
Terrain::Terrain(unsigned int w, unsigned int h, double a, const Vector & center)
{
    this->w = w + 1; // +1 pour avoir une grille de une case min
    this->h = h + 1; // Same
    this->a = a;
    this->center = center;
}

Terrain::~Terrain()
{
}

/// @brief Get the center of the field.
/// @return Vector center.
Vector Terrain::getCenter() const
{
    return center;
}


/// @brief Get the width of the field.
/// @return Int width.
unsigned int Terrain::getWidth() const
{
    return w;
}

/// @brief Get the height of the field.
/// @return Int height.
unsigned int Terrain::getHeight() const
{
    return h;
}

/// @brief Get the max altitude of the field.
/// @return double Max Altitude.
double Terrain::getAltitude() const
{
    return a;
}

/// @brief Sets the center of the field.
/// @param center Vector center.
void Terrain::setCenter(const Vector & center)
{
    this->center = center;
}

/// @brief Sets the width of the field.
/// @param width int Width.
void Terrain::setWidth(unsigned int width)
{
    w = width;
}

/// @brief Sets the width of the field.
/// @param height int Height.
void Terrain::setHeight(unsigned int height)
{
    h = height;
}

/// @brief Sets the max altitude of the field.
/// @param altitude Max altitude.
void Terrain::setAltitude(double altitude)
{
    a = altitude;
}