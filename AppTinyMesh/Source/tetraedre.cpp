#include "tetraedre.h"
/**
 * @class Tetraedre tetraedre.h
 * @brief Tetraedre volumic representation.
 */

/// @brief Construct a new Tetraedre object.
/// @param _a vertex of the tetrahedron.
/// @param _b vertex of the tetrahedron.
/// @param _c vertex of the tetrahedron.
/// @param _d vertex of the tetrahedron.
Tetraedre::Tetraedre(Vector _a, Vector _b, Vector _c, Vector _d) : a(_a), b(_b), c(_c), d(_d) {}

/// @brief Construct a new tetraedre object with vertices (1,1,1), (1,-1,1), (-1,1,-1) and (-1,-1,1).
Tetraedre::Tetraedre() : a(Vector(1,1,1)), b(Vector(1,-1,-1)), c(Vector(-1,1,-1)), d(Vector(-1,-1,1)) {}

/// @brief Construct a new Tetraedre object on a given center.
/// @param center Center of the tetrahedron.
/// @param size Distance beetween the center and the vertices.
Tetraedre::Tetraedre(Vector center, double size) {
    a = Vector(size,size,size) + center;
    b = Vector(size,-size,-size) + center;
    c = Vector(-size,size,-size) + center, 
    d = Vector(-size,-size,size) + center;
}

/// @brief Get the first vertex of the tetrahedron.
/// @return Vector vertex.
Vector Tetraedre::getA() const { return a; }

/// @brief Get the second vertex of the tetrahedron.
/// @return Vector vertex.
Vector Tetraedre::getB() const { return b; }

/// @brief Get the third vertex of the tetrahedron.
/// @return Vector vertex.
Vector Tetraedre::getC() const { return c; }

/// @brief Get the fourth vertex of the tetrahedron.
/// @return Vector vertex.
Vector Tetraedre::getD() const { return d; }